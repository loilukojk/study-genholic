'use strict';
//var { limitQuery } = require('../class/lib');

module.exports = function (Product) {
  require('../classes/product')(Product);

  //limitQuery.limitDataPerRequest(Brand);

  Product.getCategory = async (req, res, product_id, cb) => {
    try {
      let product = new Product();
      return await product.getCategory(req, res, product_id);
    } catch (err) {
      cb(err);
    }
  }

  Product.getProductWithSale = async (req, res, cb) => {
    try {
      let product = new Product();
      return await product.getProductWithSale(req, res);
    } catch (err) {
      cb(err);
    }
  }

  Product.remoteMethod('getCategory', {
    description: 'Phân loại sản phẩm thông qua thành phần của nó',
    http: { path: '/:product_id/get-category', verb: 'get' },
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'res', type: 'object', http: { source: 'res' } },
      { arg: 'product_id', type: 'number', http: { source: 'path' }, require: true }
    ],
    returns: { arg: 'data', type: ['array'], root: true }
  });

  Product.remoteMethod('getProductWithSale', {
    description: 'Danh sách sản phẩm với giá sale',
    http: { path: '/get-product-with-sale', verb: 'get' },
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'res', type: 'object', http: { source: 'res' } },
    ],
    returns: { arg: 'data', type: ['object'], root: true }
  });


};
