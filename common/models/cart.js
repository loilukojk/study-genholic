'use strict';

module.exports = function (Cart) {
    /**
     * This show detail of products in cart
     * @param {any} id cartId
     */

    Cart.wishlist = async function (id, callback) {
        try {
            let listItems = await Cart.app.models.Item.find({ where: { cartId: id } })
            let result = [];
            for (const instance of listItems) {
                result.push(await Cart.app.models.product.find({ where: { id: instance.productId } }))
            }
            callback(null, result)
        }
        catch (err) {
            callback(err)
        }
    }

    Cart.remoteMethod('wishlist', {
        http: { path: '/wishlist', verb: 'get' },
        accepts: { arg: 'cartId', type: 'string' },
        returns: { arg: 'wishlist', type: 'string' }
    })
};
