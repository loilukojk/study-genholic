'use strict';
//var { limitQuery } = require('../class/lib');

module.exports = function (Brand) {
  require('../classes/brand')(Brand);

  //limitQuery.limitDataPerRequest(Brand);

  Brand.fnHere = async (req, res, brand_id, cb) => {
    try {
      let brand = new Brand();
      return await brand.fnHere(req, res, brand_id);
    } catch (err) {
      cb(err);
    }
  }
 
  Brand.remoteMethod('fnHere', {
    description: 'List all the activities need approval by manager',
    http: { path: '/fnHere/:brand_id', verb: 'get' },
    accepts: [
      { arg: 'req', type: 'object', http: { source: 'req' } },
      { arg: 'res', type: 'object', http: { source: 'res' } },
      { arg: 'brand_id', type: 'string', http: { source: 'path' }, required: true },
    ],
    returns: { arg: 'data', type: ['object'], root: true }
  });
 
   
};
