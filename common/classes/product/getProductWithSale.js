var _ = require('lodash');
module.exports = Product => {
  Product.prototype.getProductWithSale = (req, res, cb) => {
    return new Promise(async (resolve, reject) => {
      try {
        let app = Product.app;
        let Sale = app.models.Sale;
        let products = await Product.find();
        for (const i of products) {
          let saleOfProduct = await Sale.find({ where: { product_id: i.product_id } });
          i.salePrice = i.price;
          if (_.isEmpty(saleOfProduct)) {
            i.salePercent = 0;
            continue;
          }
          for (const j of saleOfProduct) {
            i.salePrice *= percent;
            i.salePercent += j.percent
          }
        }


        resolve(products);
      } catch (error) {
        reject(error);
      }
    })
  }
}