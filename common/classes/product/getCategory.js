var _ = require('lodash');
module.exports = Product => {
  Product.prototype.getCategory = (req, res, product_id, cb) => {
    return new Promise(async (resolve, reject) => {
      try {
        let app = Product.app;
        let product_ingredient = app.models.product_ingredient;
        let Category = app.models.Category
        let ingredient_category = app.models.ingredient_category;

        let result = [];
        let ingredientOfProduct = await product_ingredient.find({ where: { product_id: product_id } });
        for (const i of ingredientOfProduct) {
          let categoryOfIngredient = await ingredient_category.find({ where: { ingredient_id: i.ingredient_id } });
          for (const j of categoryOfIngredient) {
            await result.push(await Category.find({ fields: { name: true, category_id: true, _id: false } }, { where: { category_id: j.category_id } }))
          }
        }
        resolve(result);
      } catch (error) {
        reject(error);
      }
    })
  }
}