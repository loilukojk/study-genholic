module.exports = Product => {
  require('./getCategory')(Product);
  require('./getProductWithSale')(Product);
}