'use strict';

var { success } = require('../../services/returnToUser');
var { setup } = require('../../config/setup');

module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/', server.loopback.status());
  server.use(router);
};
