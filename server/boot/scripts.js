'use strict';

var { success } = require('../../services/returnToUser');
var { setup } = require('../../config/setup');

module.exports = async (app) => {
  app.get('/setup', async (req, res, next) => {
    try {
      return success(res, "Done", await setup(app.models))
    } catch (err) {
      console.log(err);
      next();
    }
  })
}