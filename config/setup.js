"use strict"
var insRole = require('../ModelsDatabase/role')
var insBrand = require('../ModelsDatabase/brand')
var insCategory = require('../ModelsDatabase/category')
var insIngredient = require('../ModelsDatabase/ingredient')
var insIngredient_category = require('../ModelsDatabase/ingredient_category')
var insProduct = require('../ModelsDatabase/product')
var insSkin = require('../ModelsDatabase/skin')
var insType = require('../ModelsDatabase/type')
var insProduct_ingredient = require('../ModelsDatabase/product_ingredient')
var insUser = require('../ModelsDatabase/user')
var insUser_role = require('../ModelsDatabase/user_role')
module.exports = {
  setup: (models) => {
    return new Promise(async (resolve, reject) => {
      try {
        let Role = models.Role;
        let Brand = models.Brand;
        let Category = models.Category;
        let Ingredient = models.Ingredient;
        let ingredient_category = models.ingredient_category
        let Product = models.Product
        let Skin = models.Skin
        let Type = models.Type
        let product_ingredient = models.product_ingredient
        let user = models.user
        let user_role = models.user_role
        
        await console.log(await Role.create(insRole))
        await console.log(await Brand.create(insBrand));
        await console.log(await Category.create(insCategory))
        await console.log(await Ingredient.create(insIngredient))
        await console.log(await ingredient_category.create(insIngredient_category))
        await console.log(await Product.create(insProduct))
        await console.log(await Skin.create(insSkin))
        await console.log(await Type.create(insType))
        await console.log(await product_ingredient.create(insProduct_ingredient))
        await console.log(await user.create(insUser))
        await console.log(await user_role.create(insUser_role))
        resolve("done")
      }
      catch (err) {
        reject(err)
      }
    })
  }
}

